import java.io.*;
import java.net.*;
import java.util.concurrent.ExecutorService;


public class WorkerRunnable implements Runnable{

    protected 	Socket clientSocket = null;
    protected 	String serverText   = null;
    protected	ThreadPooledServer server = null;
    protected   ExecutorService threadPool = null;


    public WorkerRunnable(Socket clientSocket, String serverText, ThreadPooledServer server, ExecutorService threadPool) {
        this.clientSocket = clientSocket;
        this.serverText   = serverText;
        this.server = server;
        this.threadPool = threadPool;
    }

    public void run() {
        try {

        	BufferedReader inFromClient = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
			String	clientSentence = inFromClient.readLine();

			if(clientSentence.equals("KILL_SERVICE")) {
				OutputStream output = clientSocket.getOutputStream();
				String reply = "Stopping Server";
				output.write(reply.getBytes()); output.close();
				this.threadPool.shutdownNow();		// shut down pool
				this.server.stop();					// stop server
			}
			else if(clientSentence.length() > 5 && clientSentence.substring(0, 5).equals("HELO ")){
				// HELO text message
				InetAddress i = this.clientSocket.getLocalAddress();	// get Local info IP/Name
				OutputStream output = clientSocket.getOutputStream();

				String reply = clientSentence + "\nIP:"+ i.getHostAddress().toString() +
				"\nPort:" + this.clientSocket.getLocalPort() + "\nStudentID:11705259";
				
				output.write(reply.getBytes()); 
				output.close();
				
			}
			else {
				processOtherMessage(clientSentence);	// other messages
			}

            inFromClient.close();
        } catch (Exception e) {
            //report exception somewhere.
            e.printStackTrace();
        }
    }
    // other messages method
	private void processOtherMessage(String clientSentence) throws Exception {
		/* do nothing for now */
		
		//OutputStream output = clientSocket.getOutputStream();
		//String reply = "Message received: " + clientSentence;
		//output.write(reply.getBytes());
		//output.close();
	}
}
